import utils from "../utils/utils";
import { Config}  from "../../config";
import {app, Menu, shell} from "electron";

export class CreatedMenu {
  private webContents: any;
  private templateMenu = [
    {
      label: app.name,
      submenu: [
        { label: `关于 ${app.name}`, role: 'about' },
        { type: 'separator' },
        { label: '服务', role: 'services' },
        { type: 'separator' },
        { label: `隐藏 ${app.name}`, role: 'hide' },
        { role: 'hideothers' },
        { label: '隐藏其他', role: 'unhide' },
        { type: 'separator' },
        { label: `退出${app.name}`, role: 'quit' }
      ]
    },
    {
      label: '文件',
      submenu: [
        {
          label: '打开Html文件',
          role: 'open',
          click: async () => {
            // @ts-ignore
            this.webContents.send('TouchBarHtmlFileChoice', await utils.OpenDialog(Config.FileType.HtmlFileConfig))
          }
        },
        {
          label: '打开Pug/Jade文件',
          role: 'open',
          click: async () => {
            // @ts-ignore
            this.webContents.send('TouchBarPugFileChoice', await utils.OpenDialog(Config.FileType.PugFileConfig))
          }
        }
      ]
    },
    {
      label: '编辑',
      submenu: [
        { label: '撤销', role: 'undo' },
        { label: '恢复', role: 'redo' },
        {type: 'separator' },
        { label: '剪切', role: 'cut' },
        { label: '复制', role: 'copy' },
        { label: '粘贴', role: 'paste' },
        {type: 'separator' },
        { label: '粘贴保留样式', role: 'pasteAndMatchStyle' },
        { label: '删除', role: 'delete' },
        { label: '全选', role: 'selectAll' },
        { type: 'separator' },
        {
          label: '听写',
          submenu: [
            { label: '开始听写', role: 'startspeaking' },
            { label: '停止听写', role: 'stopspeaking' }
          ]
        }
      ]
    },
    {
      label: '视图',
      submenu: [
        { label: '刷新', role: 'reload' },
        { label: '重置', role: 'resetzoom' },
        { label: '放大', role: 'zoomin' },
        { label: '缩小', role: 'zoomout' },
        { type: 'separator' },
        { label: '全屏', role: 'togglefullscreen' },
        { label: '切换开发人员工具', role: 'toggledevtools' },
      ]
    },
    {
      label: '窗口',
      submenu: [
        { label: '最小化', role: 'minimize' },
        { label: '最大化',role: 'zoom' },
        { label: '关闭', role: 'close' }
      ]
    },
    {
      label: '帮助',
      role: 'help',
      submenu: [
        {
          label: '了解更多',
          click: async () => {
            await shell.openExternal('https://electronjs.org')
          }
        },
        {
          label: 'GitHub主页',
          click: async () => {
            await shell.openExternal(Config.Github)
          }
        }
      ]
    }
  ]

  buildFromTemplate(webContents: any) {
    this.webContents = webContents;
    // @ts-ignore
    const successMenu =  Menu.buildFromTemplate(this.templateMenu)
    Config.isMac ? Menu.setApplicationMenu(successMenu) : Menu.setApplicationMenu(null)
  }

}

export default new CreatedMenu();
