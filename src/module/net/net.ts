import { net } from "electron";
import utils from "../utils/utils";
class Net {


  get(Url: string): Promise<any> {

    const request = net.request({
      method: 'GET',
      url: Url
    });

    return new Promise((resolve, reject) => {
      request.on('response', (response) => {
        response.on('data', (chunk) => {
          resolve(chunk.toString())
        });

        response.on('error', (error: any) => {
          utils.showErrorBox({ title: 'error', content: JSON.stringify(error)});
        });

        response.on('end', () => {
          console.log('No more data in response.')
        });
      });
      request.end();
    })
  }
}

export default new Net();
