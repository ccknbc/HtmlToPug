import { systemPreferences, nativeTheme} from "electron"
import {Config} from "@/config";
import { ipcMain } from "electron";

class ThemeChange {
  MacOsThemeChange(): void {

    ipcMain.on('getCurrentTheme', async (event, arg) => {
      event.returnValue = nativeTheme.shouldUseDarkColors
    });

    // systemPreferences.subscribeNotification(
    //   'AppleInterfaceThemeChangedNotification',
    //   function theThemeHasChanged () {
    //     console.log("主题切换的时候自动触发: ", nativeTheme.shouldUseDarkColors);
    //     webContents.send('updateAppTheme', nativeTheme.shouldUseDarkColors)
    //   }
    // );

  }
}

export default new ThemeChange()
