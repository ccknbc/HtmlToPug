import { BrowserWindow } from "electron";
import { Config } from "./config/index";
import IndexWindow from "./module/ipcMain/IndexWindow";
import CreatedMenu  from "./module/menu/menu";

export class createWindow {
    public windows: any;
    constructor() {
      // @ts-ignore
      this.windows = new BrowserWindow(Config.WelcomeConfig);
      this.Start();
    }

    public async Start() {
      CreatedMenu.buildFromTemplate(this.windows.webContents);
      this.SetLoadFile();
      this.RegistrationProcess();
    }

    // 加载启动页面
    public SetLoadFile(): void {
      this.windows.loadFile(Config.WelcomePage);
    }


    // 注册IndexWindow的ipc
    public RegistrationProcess(): void {
      IndexWindow.createdWindow(this.windows);
    }
}
