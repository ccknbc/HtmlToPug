class Welcome {

  constructor() {
    this.MySwiper = null;
    this.initSwipe();
    this.startAppIndex()
    this.skipWelcome()
  }


  initSwipe() {
    this.MySwiper = new Swiper ('.swiper-container', {
      direction: 'horizontal', // 垂直切换选项
      // 如果需要分页器
      pagination: {
        el: '.swiper-pagination',
      },

      // 如果需要前进后退按钮
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })
  }

  startAppIndex() {
    $(".start h2").on('click', function () {
      $ipc.openIndex("index")
    })
  }

  skipWelcome () {
    $(".skip").on('click', () => {
      $ipc.openIndex("index")
      // this.MySwiper.slideTo(5, 1000, false);
    })
  }
}

new Welcome()
