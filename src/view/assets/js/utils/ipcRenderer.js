const { ipcRenderer } = require('electron');

class IpcRenderer {

  openFile(arg) {
    return ipcRenderer.sendSync('openFile', arg)
  }

  openSetting(arg) {
    return ipcRenderer.sendSync('openWindow', arg)
  }

  openIndex(arg) {
    return ipcRenderer.sendSync('openIndexWindow', arg)
  }

  getCurrentTheme() {
    return ipcRenderer.sendSync('getCurrentTheme')
  }

  closedSetting() {
    return ipcRenderer.sendSync('closedWindow')
  }

  isLoginStart(isStart) {
    return ipcRenderer.sendSync('settingLoginStart', isStart)
  }

  getCurrentVersion() {
    return ipcRenderer.sendSync('getVersion')
  }

  checkUpdate() {
    return ipcRenderer.sendSync('update')
  }

  downNew(arg) {
    return ipcRenderer.sendSync('downVersion', arg)
  }

  async TouchBarFileUpload() {
    // TouchBar 的pug文件选择
    ipcRenderer.on('TouchBarPugFileChoice', async (event, message) => {
      $utils.clearText();
      $(".pug").val(message);
      $(".html").val(await $utils.PugToHtml());
    })

    // TouchBar 的html文件选择
    ipcRenderer.on('TouchBarHtmlFileChoice', async (event, message) => {
      $utils.clearText();
      $(".html").val(message);
      $(".pug").val(await $utils.HtmlToPug());
    });
  }

}

window.$ipc = new IpcRenderer()
