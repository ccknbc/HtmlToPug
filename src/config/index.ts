import path from "path";

export class Config {
  // 是否为Mac系统
  public static isMac: Boolean = process.platform === 'darwin';
  // Github主页
  public static Github: string = "https://github.com/helpcode";
  // 应用升级地址
  public static UpdateUrl: string = "http://192.168.0.101:9000/checkUpload?v=";
  // 首页
  public static StartUpPage: String = path.join(__dirname, "../view/index.html");
  // 设置页面
  public static SettingPage: String = path.join(__dirname, "../view/setting.html");
  // 欢迎页面
  public static WelcomePage: String = path.join(__dirname, "../view/welcome.html");
  // 用户下载新版应用时候选择的保存路径
  public static TemporarySavePath: String;
  // 应用的当前版本，每次打包发布前需要改这个地址
  public static currentVersion: String = '1.0.3'

  // 欢迎页面的窗口配置
  public static WelcomeConfig: Object = {
    width: 250,
    height: 316,
    // titleBarStyle: 'hidden',
    frame: false,
    transparent: true,
    webPreferences: {
      nodeIntegration: true
    }
  };

  // 首页页面的窗口配置
  public static WindowsConfig: Object = {
    width: 1200,
    height: 760,
    frame: false,
    transparent: true,
    // titleBarStyle: 'hidden',
    webPreferences: {
      nodeIntegration: true
    }
  };

  // 设置页面页面的窗口配置
  public static SettingWindow: Object = {
    width: 607,
    height: 427,
    frame: false,
    parent: null,
    modal: true,
    show: false,
    resizable: false,
    closable: true,
    transparent: true,
    webPreferences: {
      nodeIntegration: true
    }
  };

  public static FileType: object = {
      HtmlFileConfig: {
          title: '请选择HTML文件',
          filters: [
              { name: 'Html', extensions: ['html', 'htmlx'] }
          ],
          properties: ['openFile']
      },
      PugFileConfig: {
          title: '请选择Pug/jade文件',
          filters: [
              { name: 'Pug', extensions: ['jade', 'pug'] }
          ],
          properties: ['openFile']
      }
  }
}
