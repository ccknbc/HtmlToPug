const gulp = require('gulp')
// 编译jade文件为hrml
const jade = require('gulp-jade')
// gulp 文件监听
const watch = require('gulp-watch')
// 处理gulp错误，防止程序报错终止
const plumber = require('gulp-plumber')
const path = require('path')

/**
 * 编译jade为html
 */
gulp.task('jade', function () {
    return gulp.src("./src/view/main/*.jade")
        .pipe(plumber({
            errHandler: e => {
                gutil.beep()
                gutil.log(e)
            }
        }))
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest("./dist/view/"))
})

gulp.task('copy',  function() {
    return gulp.src(['src/view/assets/**/*'], { base: 'src/' })
      .pipe(gulp.dest('dist/'))
});


/**
 * 监听用户在 src/ 的所有文件操作
 */
gulp.task('file', function () {
    gulp.watch('src/view/**/*', gulp.parallel('jade'))
    gulp.watch('src/view/assets/**/*', gulp.parallel('copy'))
});

gulp.task('default', gulp.series('file'))