class Index {
  constructor() {

    $utils.changeTheme($ipc.getCurrentTheme());

    if(localStorage.getItem("currentFont") !== null) {
      $utils.setFont(localStorage.getItem("currentFont"));
    }

    // if(localStorage.getItem("currentColor") !== null) {
    //   $utils.setColor(localStorage.getItem("currentColor"));
    // }

    if(localStorage.getItem("themeIndex") !== null){
      if(localStorage.getItem("CustomTheme") !== null) {
        $utils.setTheme(JSON.parse(localStorage.getItem("CustomTheme")))
      } else {
        $utils.setTheme($config.themeColor[localStorage.getItem("themeIndex")])
      }
    }


    this.bindHtmlInput();
    this.bindPugInput();

    this.HtmlFileUpload();
    this.PugFileUpload();
    this.clearInput();
    this.openSetting();

    this.MonitorFontSetting();
    this.MonitorColorSetting();
    this.MonitorThemeSetting();

    $utils.CurrentTime();
    $utils.BatteryChange();

    $ipc.TouchBarFileUpload();
  }



  /**
   * 为左边html输入框绑定input事件监听
   * @returns {Promise<void>}
   */
  async bindHtmlInput() {
    $(".html").bind("input propertychange", async () => {
      $(".html").val().length === 0 ? $(".pug").val("") : '';
      $(".pug").val(await $utils.HtmlToPug());
    });
  }

  /**
   * 为右边pug输入框绑定input事件监听
   * @returns {Promise<void>}
   */
  async bindPugInput() {
    $(".pug").bind("input propertychange", async () => {
      $(".pug").val().length === 0 ? $(".html").val("") : '';
      $(".html").val(await $utils.PugToHtml());
    });
  }

  /**
   * 状态栏Html图标被点击
   * @returns {Promise<void>}
   * @constructor
   */
  async HtmlFileUpload() {
    $(".openHtmlFile").on("click", async () => {
      $utils.clearText();
      $(".html").val($ipc.openFile("html"));
      $(".pug").val(await $utils.HtmlToPug());
    });
  }

  /**
   * 状态栏Pug图标被点击
   * @returns {Promise<void>}
   * @constructor
   */
  async PugFileUpload() {
    $(".openPugFile").on("click", async () => {
      $utils.clearText();
      $(".pug").val($ipc.openFile("pug"));
      $(".html").val(await $utils.PugToHtml());
    });
  }

  /**
   * 清除左右两个输入框
   */
  clearInput() {
    $(".deleteText").on("click", async () => $utils.clearText());
  }

  /**
   * 状态栏设置图标被点击
   * 打开设置页面
   */
  openSetting() {
    $(".openSetting").on("click", async () => $ipc.openSetting("打开设置"));
  }

  /**
   * 首页监听设置页面字体设置发送过来的广播消息
   * @constructor
   */
  MonitorFontSetting() {
    $broadCast.fontSetting.onmessage = function (e) {
      $utils.setFont(e.data);
    };
  }

  /**
   * 首页监听设置页面颜色设置发送过来的广播消息
   * @constructor
   */
  MonitorColorSetting() {
    $broadCast.ColorSetting.onmessage = function (e) {
      $utils.setColor(e.data);
    };
  }

  /**
   * 首页监听设置页面主题设置发送过来的广播消息
   * @constructor
   */
  MonitorThemeSetting() {
    $broadCast.ThemeSetting.onmessage = function (e) {
      $utils.setTheme(e.data);
    };
  }

}

new Index();


//
// // 接口后端主动的rpc通知来更改主题
// ipcRenderer.on('updateAppTheme',  function (event, isDarkMode) {
//   console.log("isDarkMode: ", isDarkMode)
//   changeTheme(isDarkMode)
// });



