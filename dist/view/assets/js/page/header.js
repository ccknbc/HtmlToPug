const {ipcRenderer: ipc} = require('electron');

$(".closed-action").click(()=> {
  ipc.send('close');
});

$(".minimize-action").click(()=> {
  ipc.send('min')
});

$(".maximization-action").click(()=> {
  ipc.send('max')
});
