const html2jade = require('html2jade');
const pug = require('pug');

class Utils {
  constructor() {

    this.MonitoringBattery()
  }

  /**
   * 设置主页字体
   * @param size
   */
  setFont(size) {
    if(size == "null") {
      $(".f").css({
        "font-size": '15px'
      })
    } else {
      console.log("size: ", size)
      $(".f").css({
        "font-size": size
      })
    }
  }

  /**
   * 设置颜色
   * @param color
   */
  setColor(color) {
    if(color != "null") {
      $(".f").css({
        "color": color
      })
    }
  }

  /**
   * 设置主题色
   * @param e
   */
  setTheme(e) {
    $(".left,.html").css({ background: e.left });
    $(".html").css({ color: e.color });
    $(".pug").css({ background: e.right, color: e.color })
    $(".times").css({ color: e.color })
  }


  /**
   * 改变主题
   * @param theme
   */
  changeTheme(theme) {
    if (theme) {
      $("body").addClass("dart")
    } else {
      $("body").removeClass("dart")
    }
  }


  /**
   * 实现时间动态显示
   * @constructor
   */
  CurrentTime() {
    setInterval(() => {
      let time = new Date();
      let t = `${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
      $(".times").text(t)
    }, 1000);
  }

  /**
   * 使用原生JS提供的方法，获取系统电量
   * @returns {Promise<string>}
   */
  async getBattery() {
    let battery = await navigator.getBattery()
    return Math.round(battery.level * 100)
  }

  /**
   * 填充电池的宽度和电量数字
   * @constructor
   */
  BatteryChange() {
    this.getBattery().then(res => {
      $(".power_quantity").text(`${res}`)
      $(".power_fill").css({
        width: `${res}%`
      })
    });
  }

  /**
   * 监听电池变化，主动触发 BatteryChange
   * 方法改变页面电池样式
   * @constructor
   */
  MonitoringBattery () {
    navigator.getBattery().then((battery) => battery.addEventListener('levelchange', () => this.BatteryChange()));
  }

  /**
   * Html转Pug语法
   * @returns {Promise<unknown>}
   * @constructor
   */
  HtmlToPug() {
    return new Promise((resolve, reject)=> {
      html2jade.convertHtml($(".html").val(), {
        nspaces: 2,        // 生成html后的空格
        noemptypipe: true, // 解决中文被编码的问题，不要中文编码
        bodyless: true,    // 解决生成的pug代码包含 html body
        donotencode: true  // 去除转换后带有的 |
      }, function (err, pug) {
        resolve(pug)
      });
    })
  }

  /**
   * Pug 转Html语法
   * @returns {Promise<unknown>}
   * @constructor
   */
  PugToHtml() {
    return new Promise((resolve, reject)=> {
      pug.render($(".pug").val(), { pretty: true }, (err,html) => {
        resolve(html)
      });
    })
  }


  /**
   * 清空输入框
   */
  clearText() {
    $(".pug").val("");
    $(".html").val("");
  };

  changeCustomTheme(e) {
    $(".leftColor").val(e.left);
    $(".rightColor").val(e.right);
    $(".fontColor").val(e.color);
  }
}

window.$utils = new Utils();
