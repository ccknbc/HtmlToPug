"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var path_1 = tslib_1.__importDefault(require("path"));
var Config = /** @class */ (function () {
    function Config() {
    }
    // 是否为Mac系统
    Config.isMac = process.platform === 'darwin';
    // Github主页
    Config.Github = "https://github.com/helpcode";
    // 应用升级地址
    Config.UpdateUrl = "http://192.168.0.101:9000/checkUpload?v=";
    // 首页
    Config.StartUpPage = path_1.default.join(__dirname, "../view/index.html");
    // 设置页面
    Config.SettingPage = path_1.default.join(__dirname, "../view/setting.html");
    // 欢迎页面
    Config.WelcomePage = path_1.default.join(__dirname, "../view/welcome.html");
    // 应用的当前版本，每次打包发布前需要改这个地址
    Config.currentVersion = '1.0.3';
    // 欢迎页面的窗口配置
    Config.WelcomeConfig = {
        width: 250,
        height: 316,
        // titleBarStyle: 'hidden',
        frame: false,
        transparent: true,
        webPreferences: {
            nodeIntegration: true
        }
    };
    // 首页页面的窗口配置
    Config.WindowsConfig = {
        width: 1200,
        height: 760,
        frame: false,
        transparent: true,
        // titleBarStyle: 'hidden',
        webPreferences: {
            nodeIntegration: true
        }
    };
    // 设置页面页面的窗口配置
    Config.SettingWindow = {
        width: 607,
        height: 427,
        frame: false,
        parent: null,
        modal: true,
        show: false,
        resizable: false,
        closable: true,
        transparent: true,
        webPreferences: {
            nodeIntegration: true
        }
    };
    Config.FileType = {
        HtmlFileConfig: {
            title: '请选择HTML文件',
            filters: [
                { name: 'Html', extensions: ['html', 'htmlx'] }
            ],
            properties: ['openFile']
        },
        PugFileConfig: {
            title: '请选择Pug/jade文件',
            filters: [
                { name: 'Pug', extensions: ['jade', 'pug'] }
            ],
            properties: ['openFile']
        }
    };
    return Config;
}());
exports.Config = Config;
