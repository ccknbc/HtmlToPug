"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var path_1 = tslib_1.__importDefault(require("path"));
var LoginStart = /** @class */ (function () {
    function LoginStart() {
    }
    LoginStart.prototype.openLoginStart = function () {
        var _this = this;
        electron_1.ipcMain.on('settingLoginStart', function (event, arg) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var appFolder, updateExe, exeName;
            return tslib_1.__generator(this, function (_a) {
                appFolder = path_1.default.dirname(process.execPath);
                updateExe = path_1.default.resolve(appFolder, '..', 'PugToHtml.app');
                exeName = path_1.default.basename(process.execPath);
                electron_1.app.setLoginItemSettings({
                    openAtLogin: arg,
                    path: updateExe,
                    args: [
                        '--processStart', "\"" + exeName + "\"",
                        '--process-start-args', "\"--hidden\""
                    ]
                });
                event.returnValue = "success";
                return [2 /*return*/];
            });
        }); });
    };
    return LoginStart;
}());
exports.default = new LoginStart();
