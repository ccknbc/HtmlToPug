"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var WindowAction = /** @class */ (function () {
    function WindowAction() {
    }
    /**
     * 自定义窗口关闭，最小化，最大化
     * @param win
     */
    WindowAction.prototype.action = function (win) {
        electron_1.ipcMain.on('min', function (e) { return win.minimize(); });
        electron_1.ipcMain.on('max', function (e) {
            if (win.isMaximized()) {
                win.unmaximize();
            }
            else {
                win.maximize();
            }
        });
        electron_1.ipcMain.on('close', function (e) { return win.close(); });
    };
    return WindowAction;
}());
exports.default = new WindowAction();
