"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var fs_1 = require("fs");
var index_1 = require("../../config/index");
var path_1 = require("path");
var utils = /** @class */ (function () {
    function utils() {
    }
    /**
     * 打开弹窗获取用户选中文件路径
     * 并调用 ReadFile 方法获取文件内容
     * @param params Object 弹窗的参数配置
     * @constructor
     */
    utils.prototype.OpenDialog = function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this.ReadFile;
                        return [4 /*yield*/, electron_1.dialog.showOpenDialog(params)];
                    case 1: return [4 /*yield*/, _a.apply(this, [(_b.sent()).filePaths[0]])];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    utils.prototype.SaveDialog = function (parent, params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, electron_1.dialog.showSaveDialog(params)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    utils.prototype.showErrorBox = function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, electron_1.dialog.showErrorBox(params.title, params.content)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 读取指定路径的文件内容
     * @param path string 路径地址
     */
    utils.prototype.ReadFile = function (path) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                if (path != undefined) {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            fs_1.readFile(path, 'utf-8', function (err, data) {
                                if (err) {
                                    reject(err);
                                }
                                resolve(data.toString());
                            });
                        })];
                }
                else {
                    this.showErrorBox({ title: '错误', content: '您未选择任何文件，请选择！' });
                }
                return [2 /*return*/];
            });
        });
    };
    utils.prototype.downFile = function (windows) {
        windows.webContents.session.on('will-download', function (event, item, webContents) {
            item.setSavePath(index_1.Config.TemporarySavePath + "/" + item.getFilename());
            item.on('updated', function (event, state) {
                if (state === 'interrupted') {
                    console.log('下载被中断，但可以继续');
                }
                else if (state === 'progressing') {
                    // 显示进度条
                    windows.setProgressBar(item.getReceivedBytes() / item.getTotalBytes());
                    if (item.isPaused()) {
                        console.log('下载已暂停');
                    }
                    else {
                    }
                }
            });
            item.once('done', function (event, state) {
                if (state === 'completed') {
                    // 下载成功后显示通知
                    var not = new electron_1.Notification({
                        title: '下载提示',
                        body: "\u6587\u4EF6 " + item.getFilename() + " \u5DF2\u6210\u529F\u4E0B\u8F7D\uFF0C\u8BF7\u53CA\u65F6\u5B89\u88C5\uFF01\uFF01",
                        silent: true,
                        icon: path_1.join(__dirname, '../../view/assets/img/pic.png'),
                        sound: path_1.join(__dirname, '../../view/assets/audio/Ping.aiff')
                    });
                    not.show();
                    windows.setProgressBar(-1);
                }
                else {
                    console.log("\u4E0B\u8F7D\u5931\u8D25: " + state);
                }
            });
        });
    };
    return utils;
}());
exports.default = new utils();
