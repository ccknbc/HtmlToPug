"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var systemPreferences = require('electron').systemPreferences;
var ThemeChange = /** @class */ (function () {
    function ThemeChange() {
    }
    ThemeChange.prototype.MacOsThemeChange = function (webContents) {
        systemPreferences.subscribeNotification('AppleInterfaceThemeChangedNotification', function theThemeHasChanged() {
            console.log("主题切换的时候自动触发");
            //updateMyAppTheme(systemPreferences.isDarkMode())
        });
    };
    return ThemeChange;
}());
exports.default = new ThemeChange();
