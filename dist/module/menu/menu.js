"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var utils_1 = tslib_1.__importDefault(require("../utils/utils"));
var config_1 = require("../../config");
var electron_1 = require("electron");
var CreatedMenu = /** @class */ (function () {
    function CreatedMenu() {
        var _this = this;
        this.templateMenu = [
            {
                label: electron_1.app.name,
                submenu: [
                    { label: "\u5173\u4E8E " + electron_1.app.name, role: 'about' },
                    { type: 'separator' },
                    { label: '服务', role: 'services' },
                    { type: 'separator' },
                    { label: "\u9690\u85CF " + electron_1.app.name, role: 'hide' },
                    { role: 'hideothers' },
                    { label: '隐藏其他', role: 'unhide' },
                    { type: 'separator' },
                    { label: "\u9000\u51FA" + electron_1.app.name, role: 'quit' }
                ]
            },
            {
                label: '文件',
                submenu: [
                    {
                        label: '打开Html文件',
                        role: 'open',
                        click: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var _a, _b, _c;
                            return tslib_1.__generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        // @ts-ignore
                                        _b = (_a = this.webContents).send;
                                        _c = ['TouchBarHtmlFileChoice'];
                                        return [4 /*yield*/, utils_1.default.OpenDialog(config_1.Config.FileType.HtmlFileConfig)];
                                    case 1:
                                        // @ts-ignore
                                        _b.apply(_a, _c.concat([_d.sent()]));
                                        return [2 /*return*/];
                                }
                            });
                        }); }
                    },
                    {
                        label: '打开Pug/Jade文件',
                        role: 'open',
                        click: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var _a, _b, _c;
                            return tslib_1.__generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        // @ts-ignore
                                        _b = (_a = this.webContents).send;
                                        _c = ['TouchBarPugFileChoice'];
                                        return [4 /*yield*/, utils_1.default.OpenDialog(config_1.Config.FileType.PugFileConfig)];
                                    case 1:
                                        // @ts-ignore
                                        _b.apply(_a, _c.concat([_d.sent()]));
                                        return [2 /*return*/];
                                }
                            });
                        }); }
                    }
                ]
            },
            {
                label: '编辑',
                submenu: [
                    { label: '撤销', role: 'undo' },
                    { label: '恢复', role: 'redo' },
                    { type: 'separator' },
                    { label: '剪切', role: 'cut' },
                    { label: '复制', role: 'copy' },
                    { label: '粘贴', role: 'paste' },
                    { type: 'separator' },
                    { label: '粘贴保留样式', role: 'pasteAndMatchStyle' },
                    { label: '删除', role: 'delete' },
                    { label: '全选', role: 'selectAll' },
                    { type: 'separator' },
                    {
                        label: '听写',
                        submenu: [
                            { label: '开始听写', role: 'startspeaking' },
                            { label: '停止听写', role: 'stopspeaking' }
                        ]
                    }
                ]
            },
            {
                label: '视图',
                submenu: [
                    { label: '刷新', role: 'reload' },
                    { label: '重置', role: 'resetzoom' },
                    { label: '放大', role: 'zoomin' },
                    { label: '缩小', role: 'zoomout' },
                    { type: 'separator' },
                    { label: '全屏', role: 'togglefullscreen' },
                    { label: '切换开发人员工具', role: 'toggledevtools' },
                ]
            },
            {
                label: '窗口',
                submenu: [
                    { label: '最小化', role: 'minimize' },
                    { label: '最大化', role: 'zoom' },
                    { label: '关闭', role: 'close' }
                ]
            },
            {
                label: '帮助',
                role: 'help',
                submenu: [
                    {
                        label: '了解更多',
                        click: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, electron_1.shell.openExternal('https://electronjs.org')];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); }
                    },
                    {
                        label: 'GitHub主页',
                        click: function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, electron_1.shell.openExternal(config_1.Config.Github)];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); }
                    }
                ]
            }
        ];
    }
    CreatedMenu.prototype.buildFromTemplate = function (webContents) {
        this.webContents = webContents;
        // @ts-ignore
        var successMenu = electron_1.Menu.buildFromTemplate(this.templateMenu);
        config_1.Config.isMac ? electron_1.Menu.setApplicationMenu(successMenu) : electron_1.Menu.setApplicationMenu(null);
    };
    return CreatedMenu;
}());
exports.CreatedMenu = CreatedMenu;
exports.default = new CreatedMenu();
