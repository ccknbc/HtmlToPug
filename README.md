<p align="center">
    <img src="./buildAssets/icons/pic.png" width="150px" height="150px"/>
</p>
<p align="center">
  PutToHtml（开发中，未正式发布...）
</p>
<p align="center">
  <a href="https://nodejs.org/en/download/"><img src="https://img.shields.io/badge/node.js-12.13.1-blue.svg"
                                                           alt="nodejs"></a>
            <a href="https://www.tslang.cn/"><img src="https://img.shields.io/badge/typescript-3.7.2-red.svg"
                                                       alt="typescript"></a>
            <a href="https://electronjs.org/"><img src="https://img.shields.io/badge/electronjs-7.1.2-yellow.svg" alt="electronjs"></a>
</p>

## 简介

`PugToHtml` 是一款跨平台的`Pug`转换工具，基于`Typescript`, `electron`进行开发。它支持主流的`Linux` / `Mac` / `Windows` 等操作系统，在各个系统上具有统一优秀的表现力。界面设计采用简洁扁平大量留白的UI风格，遵守简单简约但是功能丰富的设计原则。让用户沉浸在代码的编辑中不受叨扰。同时支持丰富的样式自定义，开机自启，电量显示，实时时间显示等。

> 满足你张扬的个性。

## 已实现功能

- 1: 启动欢迎页
- 2: 实时电量显示
- 3: 实时时间显示
- 4: Html, Pug/Jade 文件本地上传，相互转换
- 5: MacOs TouchBar 快速操作
- 6: 清空输入框
- 7: 主题样式切换
- 8: 字体大小设置
- 9: 自定义主题样式设置
- 10: 开机自启（window 上有bug，MacOs正常，Linux各发行版本未测试）
- 11: 检查更新


**项目地址：**

- 1：源码地址：https://github.com/helpcode/PugToHtml
- 2：Mac 下载：[http://static.geekhelp.cn/PugToHtml-1.0.3-mac.7z](http://static.geekhelp.cn/PugToHtml-1.0.3-mac.7z)
- 3：Win 下载：[http://static.geekhelp.cn/PugToHtml-1.0.4-win.7z](http://static.geekhelp.cn/PugToHtml-1.0.4-win.7z)
- 4：Linux 下载【暂无打包...如果有需要请Github留言】
 
 
## 截图欣赏

说得再多无图无真相，不说了，下面先放图！

1：MacOs dmg 安装包
<p align="center">
    <img width="90%" height="90%" src="./buildAssets/img/1.png"/>
</p>

2：欢迎页面-1
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/2.png"/>
</p>

3：欢迎页面-2
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/3.png"/>
</p>

4：首页-默认主题 默认白
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/4.png"/>
</p> 
 
 
5：首页-默认主题 默认黑
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/5.png"/>
</p> 
 
6：设置页面-主题切换 治愈绿
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/6.png"/>
</p> 
   
7：设置页面-样式设置
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/7.png"/>
</p> 
 
8：设置页面-关于软件
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/8.png"/>
</p> 
 
9：设置页面-关于PugToHtml
<p align="center">
   <img width="90%" height="90%" src="./buildAssets/img/9.png"/>
</p> 
    
## 安装构建

1：下载源码

```bash
git clone https://gitee.com/bmycode/HtmlToPug.git
``` 

2：解决依赖

```bash
cnpm i
``` 

3：Ts编译&Gulp编译(开发阶段必须运行)

```bash
npm run dev
``` 

4：查看客户端效果(开发阶段使用)

```bash
npm run el
``` 

**注意，打包项目发布需要进入`dist`文件夹先执行`cnpm i`，然后在执行下面命令，请一定记住！！！！！**

5：MacOs打包

```bash
npm run mac
``` 

6：Window打包

```bash
npm run win
``` 
